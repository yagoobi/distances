import math

def euclidean_distance(vector_1,vector_2):
    #this function measures the euclidean distance beween vector_1 and vector_2
    dimension=len(vector_1)
    
    s=0
    for i in range(dimension):
        s+=(vector_1[i]-vector_2[i])**2
    
    return math.sqrt(s)

#assert euclidean_distance([3,0],[0,0])==3      #testing
def manhattan_distance(vector_1,vector_2):
    #this function measures the manhattan distance beween vector_1 and vector_2
    dimension=len(vector_1)
    
    s=0
    for i in range(dimension):
        s+=abs((vector_1[i]-vector_2[i]))**2
    
    return math.sqrt(s)