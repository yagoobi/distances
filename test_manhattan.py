import distances

def test_manhattan_example():

     vector_one = [2, -1]
     vector_two = [-2, 2]
     assert distances.manhattan_distance(vector_one, vector_two) == 5