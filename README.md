# distances

Python package for calculating various distance measures

#installation

The project can be cloned locally using the following command:

$ git clone <path>

#Using

Currently the following distance measures are implemented in the package:
Euclidean distance.

$sqrt((v1-u1)**2+(v2-u2)**2)+ ...$

#tests
The package is being tested using pytest. To run the test suite use the
command:
$ pytest test_euclidean

#Licence
The package is under the MIT license.
