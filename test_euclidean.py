import distances

def test_eclidean_example():

     vector_one = [2, -1]
     vector_two = [-2, 2]
     assert distances.euclidean_distance(vector_one, vector_two) == 5